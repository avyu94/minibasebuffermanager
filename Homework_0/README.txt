Anthony Vo 008319844
Vu Le 009523306

output = 

    java -classpath "minibase-exceptions.jar:minibase-heap.jar:minibase-diskmgr.jar:minibase-global.jar:." HeapFileScan

      Insert and scan 100 fixed-size records

      - Create a heap file

      - Add 100 records to the file


      Insert and scan 2000 fixed-size records

      - Create a heap file

      - Add 2000 records to the file

      Scan file file_100 expecting: 100 records
      Scan completed successfully.

      Scan file file_2000 expecting: 2000 records
      Scan completed successfully.

