package bufmgr;

import global.AbstractBufMgrFrameDesc;
import global.GlobalConst;
import global.PageId;

public class BufMgrFrameDesc extends global.AbstractBufMgrFrameDesc implements GlobalConst
{
	private int pinCount;
	private boolean dirtyBit;
	private int frameId;
	private int pageId;
	
	/**
	 * Constructor
	 */
	BufMgrFrameDesc(int pinCount, boolean dirtyBit, int frameId, int pageId)
	{
		this.pinCount = pinCount;
		this.dirtyBit = dirtyBit;
		this.frameId = frameId;
		this.pageId = pageId;
	}

	/**
	 * Returns the pin count of a certain frame page.
	 * 
	 * @return the pin count number.
	 */
	public int getPinCount()
	{ return pinCount; };

	/**
	 * Increments the pin count of a certain frame page when the page is pinned.
	 * 
	 * @return the incremented pin count.
	 */
	public int pin()
	{ return pinCount++; };

	/**
	 * Decrements the pin count of a frame when the page is unpinned. If the pin
	 * count is equal to or less than zero, the pin count will be zero.
	 * 
	 * @return the decremented pin count.
	 */
	public int unpin()
	{ return pinCount--; };

	/**
	 * 
	 */
	public PageId getPageNo()
	{ return null; };
	public int getPageId(){
		return pageId;
	}
	/**
	 * the dirty bit, 1 (TRUE) stands for this frame is altered, 0 (FALSE) for
	 * clean frames.
	 */
	public boolean isDirty()
	{ return dirtyBit == true; };
	/**
	 * sets the bit to dirty or clean
	 */
	public void setDirty(boolean b) {
		dirtyBit = b;
		
	}
	public int getFrameId(){
		return frameId;
	}
}
