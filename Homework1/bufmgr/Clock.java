package bufmgr;

import global.GlobalConst;
import global.AbstractBufMgr;
import global.AbstractBufMgrFrameDesc;
import global.AbstractBufMgrReplacer;

import exceptions.BufferPoolExceededException;
import exceptions.InvalidFrameNumberException;
import exceptions.PagePinnedException;
import exceptions.PageUnpinnedException;

import java.util.ArrayList;;

class ClockNode{
	boolean referenced;
	int frameNo;
	public ClockNode(boolean referenced, int frameNo){
		this.referenced = referenced;
		this.frameNo = frameNo;
	}
}

/**
 * This class should implement a Clock replacement strategy.
 */
public class Clock extends BufMgrReplacer
{
	ArrayList<ClockNode> clock = new ArrayList<ClockNode>();
	int hand = 0;
	
	public Clock() 
	{
		
	};
	
	public Clock(AbstractBufMgr b) 
	{	
		super.setBufferManager((BufMgr)b);
		
		for (int i = 0; i < mgr.getNumBuffers(); i++)
		{
			clock.add(new ClockNode(false, -1));
		}
	};
	

	
	/**
	 * Pins a candidate page in the buffer pool.
	 * 
	 * @param frameNo
	 *            frame number of the page.
	 * @throws InvalidFrameNumberException
	 *             if the frame number is less than zero or bigger than number
	 *             of buffers.
	 * @return true if successful.
	 */
	public void pin(int frameNo) throws InvalidFrameNumberException
	{
		if (frameNo < 0 || frameNo > mgr.getNumBuffers())
		{
			throw new InvalidFrameNumberException(null, "Clock - pin: invalid frame number");
		}
		for(ClockNode c : clock){
			if(c.frameNo == frameNo){
				c.referenced = true;
				return;
			}
			
		}
		for(ClockNode c : clock){
			if(c.frameNo == -1){
				c.frameNo = frameNo;
				return;
			}
			
		}
		
	};

	/**
	 * Unpins a page in the buffer pool.
	 * 
	 * @param frameNo
	 *            frame number of the page.
	 * @throws InvalidFrameNumberException
	 *             if the frame number is less than zero or bigger than number
	 *             of buffers.
	 * @throws PageUnpinnedException
	 *             if the page is originally unpinned.
	 * @return true if successful.
	 */
	public boolean unpin(int frameNo) throws InvalidFrameNumberException,
			PageUnpinnedException
	{

		return false;
	};

	/**
	 * Frees and unpins a page in the buffer pool.
	 * 
	 * @param frameNo
	 *            frame number of the page.
	 * @throws PagePinnedException
	 *             if the page is pinned.
	 */
	public void free(int frameNo) throws PagePinnedException
	{	for(ClockNode c : clock){
			if(c.frameNo == frameNo){
				c.referenced = false;
				c.frameNo = -1;
			}
		}
	};

	/** Must pin the returned frame. */
	public int pick_victim() throws BufferPoolExceededException,
			PagePinnedException
	{	ClockNode c = clock.get(hand);
		int pageNo = -1;
	
		while(c.referenced == true || c.frameNo == -1)
		{	c.referenced = false;
			if(hand>=clock.size()-1){
				hand = 0;
			}
			else{
				hand++;
			}
			
			c = clock.get(hand);
		}
		
		pageNo = c.frameNo;
		//System.out.println("PAGE REPLACED "+ pageNo);
		return pageNo;
		
	} ;

	/** Retruns the name of the replacer algorithm. */
	public String name()
	{
		return "Clock";
	};

	/**
	 * Counts the unpinned frames (free frames) in the buffer pool.
	 * 
	 * @returns the total number of unpinned frames in the buffer pool.
	 */
	public int getNumUnpinnedBuffers()
	{
//		init();
		int count = 0;
		for (int i = 0; i < this.frameTable.length; i++)
		{
			if (this.state_bit[i] != Pinned)
			{
				count++;
			}
		}
		return count;
	};
}

