package bufmgr;

import global.GlobalConst;
import global.AbstractBufMgr;
import global.AbstractBufMgrFrameDesc;
import global.AbstractBufMgrReplacer;

import java.util.ArrayList;

import exceptions.BufferPoolExceededException;
import exceptions.InvalidFrameNumberException;
import exceptions.PagePinnedException;
import exceptions.PageUnpinnedException;


/**
 * This class should implement a MRU replacement strategy.
 */
public class MRU extends BufMgrReplacer
{
	private ArrayList<Integer> replacementCandidates;
	
	public MRU() 
	{
		
	};
	
	public MRU(AbstractBufMgr b) 
	{	replacementCandidates = new ArrayList<Integer>();
		super.setBufferManager((BufMgr)b);
	};
	
	/**
	 * Pins a candidate page in the buffer pool.
	 * 
	 * @param frameNo
	 *            frame number of the page.
	 * @throws InvalidFrameNumberException
	 *             if the frame number is less than zero or bigger than number
	 *             of buffers.
	 * @return true if successful.
	 */
	public void pin(int frameNo) throws InvalidFrameNumberException
	{
		if(frameNo < 0 || frameNo >= mgr.getNumBuffers()){
			
			}
		else{
			replacementCandidates.add(frameNo);
		}
		
	};

	/**
	 * Unpins a page in the buffer pool.
	 * 
	 * @param frameNo
	 *            frame number of the page.
	 * @throws InvalidFrameNumberException
	 *             if the frame number is less than zero or bigger than number
	 *             of buffers.
	 * @throws PageUnpinnedException
	 *             if the page is originally unpinned.
	 * @return true if successful.
	 */
	public boolean unpin(int frameNo) throws InvalidFrameNumberException,
			PageUnpinnedException
		{ 
		return true;
		};

	/**
	 * Frees and unpins a page in the buffer pool.
	 * 
	 * @param frameNo
	 *            frame number of the page.
	 * @throws PagePinnedException
	 *             if the page is pinned.
	 */
	public void free(int frameNo) throws PagePinnedException
	{
		replacementCandidates.remove(replacementCandidates.indexOf(frameNo));
	};

	/** Must pin the returned frame. */
	public int pick_victim() throws BufferPoolExceededException,
			PagePinnedException
	{	
		int frameNo = replacementCandidates.get(replacementCandidates.size()-1);
		//System.out.println("FRAME REPLACED "+ frameNo);
		try {
			pin(frameNo);
		} catch (InvalidFrameNumberException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return frameNo;
	}

	/** Retruns the name of the replacer algorithm. */
	public String name()
	{ return "MRU"; };

	/**
	 * Counts the unpinned frames (free frames) in the buffer pool.
	 * 
	 * @returns the total number of unpinned frames in the buffer pool.
	 */
	public int getNumUnpinnedBuffers()
	{ return 0; };
}
